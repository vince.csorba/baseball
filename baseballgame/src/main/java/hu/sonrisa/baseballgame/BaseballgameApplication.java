package hu.sonrisa.baseballgame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseballgameApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseballgameApplication.class, args);
	}

}
