package hu.sonrisa.baseballgame;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BaseballGameApplicationTests {

    private BaseballGame game = new BaseballGame();

    @Test
    void simpleGame() {
        int result = game.calculatePoints(new String[]{"1", "1"});
        assertEquals(2, result);
    }

    @Test
    void firstGame() {
        int result = game.calculatePoints(new String[]{"5", "2", "C", "D", "+"});
        assertEquals(30, result);
    }

    @Test
    void secondGame() {
        int result = game.calculatePoints(new String[]{"5", "-2", "4", "C", "D", "9", "+", "+"});
        assertEquals(27, result);
    }

    @Test
    void thirdGame() {
        int result = game.calculatePoints(new String[]{"4", "7", "C", "C", "1", "D"});
        assertEquals(3, result);
    }

    @Test
    void fourthGame() {
        int result = game.calculatePoints(new String[]{"7", "12", "+", "C", "C"});
        assertEquals(7, result);
    }


    @Test
    void fifthGame() {
        int result = game.calculatePoints(new String[]{"2", "6", "+", "D", "5", "7", "C", "D"});
        assertEquals(47, result);
    }


    @Test
    void sixthGame() {
        int result = game.calculatePoints(new String[]{});
        assertEquals(0, result);
    }

    @Test
    void seventhGame() {
        int result = game.calculatePoints(new String[]{"-2", "-4", "+"});
        assertEquals(-12, result);
    }

}
